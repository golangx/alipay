package alipay

import (
	"encoding/json"
	"fmt"
)

// Precreate 预创建订单,生成二维码后，展示给用户
func Precreate(bizContent string) (err error) {
	res, err := trade("alipay.trade.precreate", bizContent)
	var result PayResponse
	err = json.Unmarshal(res, &result)
	if err != nil {
		err = fmt.Errorf("响应结果json解析失败:%v 响应原数据: %s", err, string(res))
		return
	}
	if result.Response.Code != "10000" {
		err = fmt.Errorf("预创建订单失败:%v 响应原数据: %s", err, string(res))
	}
	return
}
