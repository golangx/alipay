package alipay

import (
	"encoding/json"
	"testing"
)

func TestPrecreate(t *testing.T) {
	New(alipayAppID, alipayPublicKey, alipayPrivateKey)
	content := make(map[string]interface{})
	content["out_trade_no"] = "2017111512000001"
	content["scene"] = "bar_code"
	content["auth_code"] = "28763443825664394"
	content["subject"] = "Iphone6 16G"
	bizContent, err := json.Marshal(content)
	if err != nil {
		t.Error(err)
	}
	body, err := trade(string(bizContent), "alipay.trade.pay")
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(string(body))
}
