package alipay

import (
	"encoding/json"
	"fmt"
)

// CloseResponse 交易关闭响应参数
type CloseResponse struct {
	Response CloseResponseData `josn:"alipay_trade_close_response"`
	Sign     string            `json:"sign"`
}

// CloseResponseData 交易关闭响应参数内容
type CloseResponseData struct {
	Code       string `json:"code"`
	Msg        string `json:"msg"`
	TradeNO    string `json:"trade_no"`
	OutTradeNO string `json:"out_trade_no"`
	SubCode    string `json:"sub_code"`
	SubMsg     string `json:"sub_msg"`
}

// Close 统一收单交易关闭接口
func Close(bizContent string) (err error) {
	res, err := trade("alipay.trade.close", bizContent)
	var result PayResponse
	err = json.Unmarshal(res, &result)
	if err != nil {
		err = fmt.Errorf("响应结果json解析失败:%v 响应原数据: %s", err, string(res))
		return
	}
	if result.Response.Code != "10000" {
		err = fmt.Errorf("交易关闭失败, 响应数据: %s", string(res))
	}
	return
}
