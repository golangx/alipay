package alipay

import (
	"encoding/json"
	"fmt"
)

// Create 商户通过该接口进行交易的创建下单
func Create(bizContent string) (err error) {
	res, err := trade("alipay.trade.create", bizContent)
	var result PayResponse
	err = json.Unmarshal(res, &result)
	if err != nil {
		err = fmt.Errorf("响应结果json解析失败:%v 响应原数据: %s", err, string(res))
		return
	}
	if result.Response.Code != "10000" {
		err = fmt.Errorf("创建订单失败,响应数据: %s", string(res))
	}
	return
}
