package alipay

import (
	"encoding/json"
	"fmt"
)

//SettleResponse 交易结算响应参数
type SettleResponse struct {
	Response SettleResponseData `josn:"alipay_trade_order_settle_response"`
	Sign     string             `json:"sign"`
}

// SettleResponseData 交易结算响应参数内容
type SettleResponseData struct {
	Code    string `json:"code"`
	Msg     string `json:"msg"`
	TradeNO string `json:"trade_no"` //支付宝交易号
	SubCode string `json:"sub_code"`
	SubMsg  string `json:"sub_msg"`
}

// Settle 交易结算接口
func Settle(bizContent string) (err error) {
	res, err := trade("alipay.trade.order.settle", bizContent)
	var result RefundResponse
	err = json.Unmarshal(res, &result)
	if err != nil {
		err = fmt.Errorf("响应结果json解析失败:%v 响应原数据: %s", err, string(res))
		return
	}
	if result.Response.Code != "10000" {
		err = fmt.Errorf("支付失败失败:%v 响应原数据: %s", err, string(res))
	}
	return
}
