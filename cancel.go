package alipay

import (
	"encoding/json"
	"fmt"
)

// CancelResponse 交易撤销响应参数
type CancelResponse struct {
	Response CancelResponseData `josn:"alipay_trade_close_response"`
	Sign     string             `json:"sign"`
}

// CancelResponseData 交易撤销响应参数内容
type CancelResponseData struct {
	Code       string `json:"code"`
	Msg        string `json:"msg"`
	TradeNO    string `json:"trade_no"`
	OutTradeNO string `json:"out_trade_no"`
	RetryFlag  string `json:"retry_flag"` //是否需要重试 Y: 需要重试, N: 不需要
	Action     string `json:"action"`     //撤销结果 close：关闭交易，无退款 refund：产生了退款
	SubCode    string `json:"sub_code"`
	SubMsg     string `json:"sub_msg"`
}

// Cancel 统一收单交易撤销接口
// 如果此订单用户支付失败，支付宝系统会将此订单关闭
// 如果用户支付成功，支付宝系统会将此订单资金退还给用户
func Cancel(bizContent string) (err error) {
	res, err := trade("alipay.trade.cancel", bizContent)
	var result PayResponse
	err = json.Unmarshal(res, &result)
	if err != nil {
		err = fmt.Errorf("响应结果json解析失败:%v 响应原数据: %s", err, string(res))
		return
	}
	if result.Response.Code != "10000" {
		err = fmt.Errorf("交易撤销失败,响应数据: %s", string(res))
	}
	return
}
