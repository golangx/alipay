package alipay

import (
	"encoding/json"
	"testing"
)

const (
	// alipayPublicKey 支付宝公钥
	alipayPublicKey = `-----BEGIN PUBLIC KEY-----  
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmBnVm9nKLUup2jzvkW5VGEYF9Atx7oY2lNn3X1JO99q7CVmniXUnl4lXUFK5NMq7IDT8WInHlv0nD/91ruOHBrqegV6K90QXZJFAuxRcIHwbwZFpcNuGZS6dHayPtK0t5yt5SLyfAd6r6PN3J5BhpI8fRXX56cGYV0fPB4pfOznt2YNFCI//OR+WeiGDn1Xhd+u7wn+pejI5vWgRYutr9cReVyJsTSRtoT7PHISQkO7V//Umknb9pxEpwDgj/pm0k2qnLGCOn+8jYuv+31j+O8yfaJ1WTzEPQA2dmanYDoDFheO//gPIlFKSE666VDY89pXHSiAiimaJnmI8JPepxwIDAQAB
-----END PUBLIC KEY-----
`
	//alipayAppID 支付宝应用id
	alipayAppID = "2016072900118092"
	// alipayPrivateKey 支付宝私钥
	alipayPrivateKey = `-----BEGIN RSA PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCYGdWb2cotS6naPO+RblUYRgX0C3HuhjaU2fdfUk732rsJWaeJdSeXiVdQUrk0yrsgNPxYiceW/ScP/3Wu44cGup6BXor3RBdkkUC7FFwgfBvBkWlw24ZlLp0drI+0rS3nK3lIvJ8B3qvo83cnkGGkjx9FdfnpwZhXR88Hil87Oe3Zg0UIj/85H5Z6IYOfVeF367vCf6l6Mjm9aBFi62v1xF5XImxNJG2hPs8chJCQ7tX/9SaSdv2nESnAOCP+mbSTaqcsYI6f7yNi6/7fWP47zJ9onVZPMQ9ADZ2ZqdgOgMWF47/+A8iUUpITrrpUNjz2lcdKICKKZomeYjwk96nHAgMBAAECggEAVOMWNET4cQ8pXINlsOTC2Sk7aVh53YVKMQj05l4pdGq+uBkxR6X60rfz4wBIHQ6IIRFxRHigy8O9aVBJfbG+SiMdE1aQeC+oXpgyaDaUtAyP8RgWoMXj29SIG2eTTGLam4nHxrB+jayYhQpcFdiephi6eym8sJk7THrnMG4peAOe4PYCh5U0T+Mq+MN7PLw+2vuFzU1l9sGD6ddo59lGqpscvsA1HGeM+HntUsrfpzzMYxzsP+0IxRLbWydWVzFXlGe+eAHsg7taZgXW52XakiwQ9MBTeZzvIowgLE3N3mjOGC8pAq12pypU5wyLzx6pPlKSqbiYSNS1cOPf+Qo9AQKBgQDdeKdT2Xs7Y4LQiF3u4JHqBke/Gy8HHgB/2eJZYb1WfHgRHdhIFvuzkJNcmDUucwrljDXC3v7hT2ayfoeuO1va4RU6Ohbhaol5O7tQh8eJ1mb7QICnkvpo6A/9+FfppCtVpjpUAn0DPiZctSHatnatMBByKbWmBfN6J++0flr0QwKBgQCv0HmYD1W+3xQvsAn+ArBJs/nDQUX5AhrrFUbgGc059D8JLeNsWrCcuX8PbqOxDVdfGE1HugLpmRioNVobvHf6zeMRV/flKXi3IkLYIdZsnOVIi+HxvslKS3tAuBE+gwky8kVMRxJaIjRMinBMDIqIoSWT3V6aEltU2tVoUPu+LQKBgFhPB8lzaqPfpgWommvpt8TtBCa7nCDmYaSZKJwjUO/vaaDJmbgFEhAjkd7WAj6nOK0XH0i4ykDk/DooIcnjeVjpbyFskDsJ1A83NhWCTpozl5SPv70R6i9yeQ33vHY5M95PDer5owdr/LfysbEYGHIuPCCJaRjuPoty6S27RGEpAoGBAJJsSOhoVRw7A7/Xl+rR6GnplyiTkygYJb0Folp/gpkyntERQ6f5O7+7CWQb2/5Xwx1yyuHivAw6sftdO6d/5lid4e+O2OklCeFTimIghEk+vgIWZd2E7HWQuZyj79ClJyMj/KzTQCiK9g2M+ouLolC6DObB5cjhYukfu74+O/sdAoGAaCcW/3R0Xq6LcFUqfuTPoAnS2cacef5TF9hSKF78F7dd18qYPX47yZpIKuqKmqrngPGj/JwBZ14aE1ns/UsnR94yQ/Et6q863MLUH74+zhzpiMEDXkSqOy1RhWXONpQgfcwRAY1Y+Fq0aPb4ctKe8LMfA2zlnL6jnhkB0mqZKqo=
-----END RSA PRIVATE KEY-----`
)

func TestNew(t *testing.T) {
	New(alipayAppID, alipayPublicKey, alipayPrivateKey)
	t.Log(appID, publicKey, privateKey)
}

func Test_paramsFill(t *testing.T) {
	content := make(map[string]interface{})
	content["out_trade_no"] = "2017111512000001"
	content["scene"] = "bar_code"
	content["auth_code"] = "28763443825664394"
	content["subject"] = "Iphone6 16G"
	bizContent, err := json.Marshal(content)
	if err != nil {
		t.Error(err)
	}
	data := paramsFill("alipay.trade.pay", string(bizContent))
	t.Log(data)
}

func Test_tradePreCreate(t *testing.T) {
	New(alipayAppID, alipayPublicKey, alipayPrivateKey)
	content := make(map[string]interface{})
	content["out_trade_no"] = "2017111512000001"
	content["subject"] = "Iphone6 16G"
	content["total_amount"] = 88.88

	bizContent, err := json.Marshal(content)
	if err != nil {
		t.Error(err)
	}
	body, err := trade("alipay.trade.precreate", string(bizContent))
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(string(body))
}

func Test_tradeCreate(t *testing.T) {
	New(alipayAppID, alipayPublicKey, alipayPrivateKey)
	content := make(map[string]interface{})
	content["out_trade_no"] = "2017111512000001"
	content["subject"] = "Iphone6 16G"
	content["total_amount"] = 88.88

	bizContent, err := json.Marshal(content)
	if err != nil {
		t.Error(err)
	}
	body, err := trade("alipay.trade.create", string(bizContent))
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(string(body))
}

func Test_tradePay(t *testing.T) {
	New(alipayAppID, alipayPublicKey, alipayPrivateKey)
	content := make(map[string]interface{})
	content["out_trade_no"] = "bax07623rj8xdmi58uwy00c7"
	content["scene"] = "bar_code"
	content["auth_code"] = "bax07112eyvwxnbag8wl001f"
	content["subject"] = "Iphone6 16G"
	bizContent, err := json.Marshal(content)
	if err != nil {
		t.Error(err)
	}
	body, err := trade("alipay.trade.pay", string(bizContent))
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(string(body))
}
