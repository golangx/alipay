package alipay

import (
	"encoding/json"
	"fmt"
)

//PayResponse 交易支付响应参数
type PayResponse struct {
	Response PayResponseData `josn:"alipay_trade_pay_response"`
	Sign     string          `json:"sign"`
}

// PayResponseData 交易支付响应参数内容
type PayResponseData struct {
	Code    string `json:"code"`
	Msg     string `json:"msg"`
	SubCode string `json:"sub_code"`
	SubMsg  string `json:"sub_msg"`
}

// Pay 统一收单交易支付接口
func Pay(bizContent string) (err error) {
	res, err := trade("alipay.trade.pay", bizContent)
	var result PayResponse
	err = json.Unmarshal(res, &result)
	if err != nil {
		err = fmt.Errorf("响应结果json解析失败:%v 响应原数据: %s", err, string(res))
		return
	}
	if result.Response.Code != "10000" {
		err = fmt.Errorf("订单支付失败:%v 响应原数据: %s", err, string(res))
	}
	return
}

// Query 支付宝支付订单的查询
func Query(bizContent string) (err error) {
	res, err := trade("alipay.trade.query", bizContent)
	var result PayResponse
	err = json.Unmarshal(res, &result)
	if err != nil {
		err = fmt.Errorf("响应结果json解析失败:%v 响应原数据: %s", err, string(res))
		return
	}
	if result.Response.Code != "10000" {
		err = fmt.Errorf("订单查询失败,响应数据: %s", string(res))
	}
	return
}
